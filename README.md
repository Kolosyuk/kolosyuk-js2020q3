# kolosyuk-JS2020Q3

Private repository for @kolosyuk

## Ссылки на работы школы:

1. ## Webdev

- **Task** - https://github.com/rolling-scopes-school/tasks/blob/master/tasks/markups/level-1/webdev/webdev-ru.md
- **Deploy** - https://rolling-scopes-school.github.io/kolosyuk-JS2020Q3/webdev/

2. ## Shelter

- **Task** - https://github.com/rolling-scopes-school/tasks/blob/master/tasks/markups/level-2/shelter/shelter-main-page-ru.md
- **Task** - https://github.com/rolling-scopes-school/tasks/blob/master/tasks/markups/level-2/shelter/shelter-pets-page-ru.md
- **Task** - https://github.com/rolling-scopes-school/tasks/blob/master/tasks/markups/level-2/shelter/shelter-adaptive-ru.md
- **Task** -https://github.com/rolling-scopes-school/tasks/blob/master/tasks/markups/level-2/shelter/shelter-DOM-ru.md
- **Deploy** https://rolling-scopes-school.github.io/kolosyuk-JS2020Q3/shelter/pages/main/main.html

3. ## Momentum

- **Task** - https://github.com/rolling-scopes-school/tasks/blob/master/tasks/ready-projects/momentum.md
- **Deploy** - https://rolling-scopes-school.github.io/kolosyuk-JS2020Q3/momentum/

4. ## Calculator

- **Task** - https://github.com/rolling-scopes-school/tasks/blob/master/tasks/ready-projects/calculator.md
- **Deploy** - https://rolling-scopes-school.github.io/kolosyuk-JS2020Q3/calculator/

5. ## Virtual Keyboard

- **Task** - https://github.com/rolling-scopes-school/tasks/blob/master/tasks/ready-projects/virtual-keyboard.md
- **Deploy** - https://kolosyuk-virtual-keyboard.netlify.app/virtual-keyboard/

6. ## Gem puzzle

- **Task** - https://github.com/rolling-scopes-school/tasks/blob/master/tasks/gem-pazzle/codejam-the-gem-puzzle.md
- **Deploy** - https://kolosyuk-gem-puzzle.netlify.app/gem_puzzle/dist/

7. ## English for kids

- **Task** - https://github.com/rolling-scopes-school/tasks/blob/master/tasks/rslang/english-for-kids.md
- **Deploy** - https://kolosyuk-english-for-kids.netlify.app/
