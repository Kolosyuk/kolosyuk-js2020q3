// DOM elements
const today = document.querySelector("#day");
const time = document.querySelector("#time");
const greeting = document.querySelector("#greeting");
const name = document.querySelector("#name");
const focus = document.querySelector("#focus");
const blockquote = document.querySelector("blockquote");
const jokeBtn = document.querySelector("#joke");
const cityInfo = document.querySelector("#city__info");
const weatherInfo = document.querySelector("#weather__info");
const weatherIcon = document.querySelector(".weather__icon");
const changeBGBtn = document.querySelector("#btn__bg");

//Option
const showAmPm = false;

let nameTemp = "";
let focusTemp = "";
let cityTemp;
let base = ``;
let dailyList = [];
let startIndex = new Date().getHours();
let shift = 1;

const images = [
  "01.jpg",
  "02.jpg",
  "03.jpg",
  "04.jpg",
  "05.jpg",
  "06.jpg",
  "07.jpg",
  "08.jpg",
  "09.jpg",
  "10.jpg",
  "11.jpg",
  "12.jpg",
  "13.jpg",
  "14.jpg",
  "15.jpg",
  "16.jpg",
  "17.jpg",
  "18.jpg",
  "19.jpg",
  "20.jpg",
];

let i = 0;

const days = [
  "Воскресенье",
  "Понедельник",
  "Вторник",
  "Среда",
  "Четверг",
  "Пятница",
  "Суббота",
];

const months = [
  "Января",
  "Февраля",
  "Марта",
  "Апреля",
  "Мая",
  "Июня",
  "Июля",
  "Августа",
  "Сентября",
  "Октября",
  "Ноября",
  "Декабря",
];

// API for jokes

const jokesURL = `https://api.chucknorris.io/jokes/random`;
let weatherUrl = `https://api.openweathermap.org/data/2.5/weather?q=Минск&lang=ru&appid=82419b3c788ed1dd4419d635e8cf7c97&units=metric`;
let city = "";

const getData = async function (url) {
  const response = await fetch(url);
  if (!response.ok) {
    weatherInfo.innerText = "Ничего не нашлось, попробуйте другой город";
    throw new Error(`Ошибка по адресу ${url}, 
                      статус ошибки ${response.status}`);
  }
  return await response.json();
};

//Show Day
function showDay() {
  let day = new Date().getDay(),
    month = new Date().getMonth();

  today.innerHTML = `${new Date().getDate()} ${months[month]}, ${days[day]}`;
}

//SHOW Time

function showTime() {
  let today = new Date(),
    hour = today.getHours(),
    min = today.getMinutes(),
    sec = today.getSeconds();

  //Set AP or PM
  const amPM = hour >= 12 ? "PM" : "AM";

  //if new hour - change BG and greeting if necessary
  if (sec === 0 && min === 0) {
    setBackgroundAndGreet();
    startIndex = new Date().getHours(); //nullify startIndex for changeBackground()
  }

  //12hr Format

  //// hour = hour % 12 || 12;

  //Output Time

  time.innerHTML = `${hour}<span>:</span>${addZero(min)}<span>:</span>${addZero(
    sec
  )} ${showAmPm ? amPM : ""}`;

  setTimeout(showTime, 1000);
}

//Add zero

function addZero(n) {
  return (parseInt(n, 10) < 10 ? "0" : "") + n;
}

//Set background
function setBackground(hour) {
  const body = document.querySelector("body");
  const src = `${dailyList[hour]}`;
  const img = document.createElement("img");
  img.src = src;
  img.onload = () => {
    body.style.backgroundImage = `url(${dailyList[hour]})`;
  };
}

//Set background and Greeting

function setBackgroundAndGreet() {
  let today = new Date(),
    hour = today.getHours();
  if (hour < 6) {
    //Night
    setBackground(hour);
    greeting.innerText = "Доброй ночи. Может спать?";
  } else if (hour < 12) {
    //Morning
    setBackground(hour);
    greeting.innerText = "Бодрое утро!";
  } else if (hour < 18) {
    //Afternoon
    setBackground(hour);
    greeting.innerText = "Хорошего дня >.<";
  } else {
    //Evening
    setBackground(hour);
    greeting.innerText = "Добрейшего вечерочка";
  }
}

//Change background by btn

function changeBackground() {
  if (startIndex + shift >= 24) {
    startIndex = 0;
    shift = 0;
  }
  setBackground(startIndex + shift);
  ++shift;
  changeBGBtn.disabled = true;
  setTimeout(function () {
    changeBGBtn.disabled = false;
  }, 1000);
}

function createBaseForImageURL(timeOfDay) {
  base = `./img/${timeOfDay}/`;
}

function createDailyList() {
  let imageSrc;
  for (let i = 0; i < 24; i++) {
    if (i < 6) {
      createBaseForImageURL("night");
      imageSrc = base + images[Math.floor(Math.random() * 20)];
      if (!dailyList.includes(imageSrc)) {
        dailyList.push(imageSrc);
        continue;
      } else {
        i--;
        continue;
      }
    } else if (i < 12) {
      createBaseForImageURL("morning");
      imageSrc = base + images[Math.floor(Math.random() * 20)];
      if (!dailyList.includes(imageSrc)) {
        dailyList.push(imageSrc);
        continue;
      } else {
        i--;
        continue;
      }
    } else if (i < 18) {
      createBaseForImageURL("day");
      imageSrc = base + images[Math.floor(Math.random() * 20)];
      if (!dailyList.includes(imageSrc)) {
        dailyList.push(imageSrc);
        continue;
      } else {
        i--;
        continue;
      }
    } else {
      createBaseForImageURL("evening");
      imageSrc = base + images[Math.floor(Math.random() * 20)];
      if (!dailyList.includes(imageSrc)) {
        dailyList.push(imageSrc);
        continue;
      } else {
        i--;
        continue;
      }
    }
  }
}

//Set name

function setName(e) {
  switch (e.type) {
    case "click":
      nameTemp = name.innerText;
      name.innerText = "";
      break;
    case "blur":
      name.innerText = nameTemp;
      nameTemp = "";
      break;
    case "keypress":
      if (e.keyCode == 13) {
        if (e.target.innerText.trim().length === 0) {
          name.blur();
        } else {
          localStorage.setItem("name", e.target.innerText);
          nameTemp = localStorage.getItem("name");
          name.textContent = localStorage.getItem("name");
          name.blur();
        }
      }
      break;
  }
}

//Get name

function getName() {
  if (localStorage.getItem("name") === null) {
    name.textContent = "[введите имя]";
  } else {
    name.textContent = localStorage.getItem("name");
  }
}

//Set Focus

function setFocus(e) {
  switch (e.type) {
    case "click":
      focusTemp = focus.innerText;
      focus.innerText = "";
      break;
    case "blur":
      focus.innerText = focusTemp;
      focusTemp = "";
      break;
    case "keypress":
      if (e.keyCode == 13) {
        if (e.target.innerText.trim().length === 0) {
          focus.blur();
        } else {
          localStorage.setItem("focus", e.target.innerText);
          focusTemp = localStorage.getItem("focus");
          focus.textContent = localStorage.getItem("focus");
          focus.blur();
        }
      }
      break;
  }
}

//Get Focus

function getFocus() {
  if (localStorage.getItem("focus") === null) {
    focus.textContent = "Цель?";
  } else {
    focus.textContent = localStorage.getItem("focus");
  }
}

//Jokes

function newJoke() {
  getData(jokesURL).then((data) => {
    if (data.value.length <= 120) {
      blockquote.innerText = data.value;
      setTimeout(newJoke, 180000);
    } else {
      newJoke();
    }
  });
}

//Weather
// api 82419b3c788ed1dd4419d635e8cf7c97
// https://api.openweathermap.org/data/2.5/weather?q=Минск&lang=ru&appid=82419b3c788ed1dd4419d635e8cf7c97&units=metric

// Set city
function setCity(e) {
  switch (e.type) {
    case "click":
      cityTemp = cityInfo.innerText;
      cityInfo.innerText = "";
      break;
    case "blur":
      cityInfo.innerText = cityTemp;
      cityTemp = "";
      break;
    case "keypress":
      if (e.keyCode == 13) {
        if (e.target.innerText.trim().length === 0) {
          cityInfo.blur();
        } else {
          if (/[a-zа-я]/gi.test(e.target.innerText)) {
            localStorage.setItem("cityInfo", e.target.innerText);
            cityTemp = localStorage.getItem("cityInfo");
            cityInfo.textContent = localStorage.getItem("cityInfo");
            getWeather(cityInfo.textContent);
            cityInfo.blur();
          } else {
            weatherInfo.innerText = "неверное название города";
            cityInfo.blur();
          }
        }
      }
      break;
  }
}

//Get city
function getCity() {
  if (localStorage.getItem("cityInfo") === null) {
    cityInfo.innerText = "Введите город";
  } else {
    cityInfo.innerText = localStorage.getItem("cityInfo");
    getWeather(cityInfo.innerText);
  }
}

function getWeather(city) {
  weatherUrl = `https://api.openweathermap.org/data/2.5/weather?q=${city}&lang=ru&appid=82419b3c788ed1dd4419d635e8cf7c97&units=metric`;
  getData(weatherUrl).then((data) => {
    weatherIcon.className = "weather__icon weather-icon owf";
    weatherIcon.classList.add(`owf-${data.weather[0].id}`);
    weatherInfo.innerHTML = ` ${data.weather[0].description} <br>
                              Температура ${data.main.temp}C <br>
                              Ощущается как ${data.main.feels_like}C <br>
                              Влажность ${data.main.humidity} %<br>
                              Cкорость ветра ${data.wind.speed} м/с<br>
                              `;
  });
}

function init() {
  createDailyList();
  showDay();
  showTime();
  setBackgroundAndGreet();
  getName();
  getFocus();
  newJoke();
  getCity();
}

jokeBtn.addEventListener("click", newJoke);
name.addEventListener("click", setName);
name.addEventListener("keypress", setName);
name.addEventListener("blur", setName);
focus.addEventListener("keypress", setFocus);
focus.addEventListener("blur", setFocus);
focus.addEventListener("click", setFocus);
cityInfo.addEventListener("keypress", setCity);
cityInfo.addEventListener("click", setCity);
cityInfo.addEventListener("blur", setCity);
changeBGBtn.addEventListener("click", changeBackground);

//RUN//

init();
