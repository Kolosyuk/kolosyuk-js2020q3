window.SpeechRecognition =
  window.SpeechRecognition || window.webkitSpeechRecognition;

const recognition = new SpeechRecognition();
recognition.interimResults = true;

const keyboard = {
  elements: {
    main: null,
    keyContainer: null,
    keys: [],
  },

  eventHandlers: {
    oninput: null,
    onclose: null,
  },

  properties: {
    input: null,
    value: "",
    isUpperCase: false,
    isAdditionalValues: false,
    isMainLanguage: true,
    isSoundOn: true,
    isSpeachRecognition: false,
  },

  additionalRuLayout: ["!", '"', "№", ";", "%", ":", "?", "*", "(", ")"],
  additionalEngLayout: ["!", "@", "#", "$", "%", "^", "&", "*", "(", ")"],
  baseLayout: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"],

  init() {
    // create wrappers
    this.elements.main = document.createElement("div");
    this.elements.keyContainer = document.createElement("div");

    //setup wrappers
    this.elements.main.classList.add("keyboard", "keyboard_hidden");
    this.elements.keyContainer.classList.add("keyboard__keys");
    this.elements.keyContainer.appendChild(this._createKeys());

    this.elements.keys = this.elements.keyContainer.querySelectorAll(
      ".keyboard__key"
    );

    //add to dom
    document.body.appendChild(this.elements.main);
    this.elements.main.appendChild(this.elements.keyContainer);

    //auto use keyboard for elements with .use-keyboard-input

    document.querySelectorAll(".use-keyboard-input").forEach((element) => {
      element.addEventListener("focus", () => {
        this.properties.input = element;
        this.open(
          element.value,
          (currentValue) => {
            element.value = currentValue;
          },
          () => {}
        );
      });
      element.addEventListener("keydown", (e) => {
        // console.log(e);
        const key = document.querySelector(`button[data-code="${e.keyCode}"]`);
        if (!key) return;
        e.preventDefault();
        key.classList.add("keyboard__key_press_true");
        this._playSound(e.keyCode);
        if (e.keyCode === 8) {
          this._delLeft();
          return;
        }
        if (e.keyCode === 13) {
          this._addNewLetter("\n");
          return;
        }
        if (e.keyCode === 32) {
          this._addNewLetter(" ");
          return;
        }
        if (e.keyCode === 46) {
          this._delRight();
          return;
        }
        if (e.keyCode === 20) {
          this._toggleCapsLock();
          key.classList.toggle("keyboard__key_active");
          return;
        }
        if (e.keyCode === 17) {
          this._toggleCtrl();
          key.classList.toggle("keyboard__key_active");
          return;
        }
        if (e.keyCode === 18) {
          this._toggleAlt();
          key.classList.toggle("keyboard__key_active");
          return;
        }
        if (e.keyCode === 16) {
          this._toggleShift();
          key.classList.toggle("keyboard__key_active");
          return;
        }
        if (e.keyCode === 37) {
          this._move(-1);
          return;
        }

        if (e.keyCode === 38) {
          this._move("up");
          return;
        }

        if (e.keyCode === 39) {
          this._move(1);
          return;
        }

        if (e.keyCode === 40) {
          this._move("down");
          return;
        }

        this._addNewLetter(e.key);
      });
      element.addEventListener("keyup", (e) => {
        const key = document.querySelector(`button[data-code="${e.keyCode}"]`);
        if (!key) return;

        key.classList.remove("keyboard__key_press_true");

        if (e.keyCode === 16) {
          this._toggleShift();
          key.classList.toggle("keyboard__key_active");
          return;
        }
        if (e.keyCode === 17) {
          this._toggleCtrl();
          key.classList.toggle("keyboard__key_active");
          return;
        }
        if (e.keyCode === 18) {
          this._toggleAlt();
          key.classList.toggle("keyboard__key_active");
          return;
        }
      });
    });
  },

  _createKeys() {
    const fragment = new DocumentFragment();

    const engKeyLayout = [
      "`",
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "0",
      "-",
      "=",
      "backspace",
      "keyboard_capslock",
      "q",
      "w",
      "e",
      "r",
      "t",
      "y",
      "u",
      "i",
      "o",
      "p",
      "[",
      "]",
      "delete",
      "unarchive",
      "a",
      "s",
      "d",
      "f",
      "g",
      "h",
      "j",
      "k",
      "l",
      ";",
      "'",
      "\\",
      "keyboard_return",
      "keyboard_hide",
      "keyboard_voice",
      "z",
      "x",
      "c",
      "v",
      "b",
      "n",
      "m",
      ",",
      ".",
      "?",
      "keyboard_arrow_up",
      "Ctrl",
      "Eng",
      "Alt",
      "space_bar",
      "volume_off",
      "keyboard_arrow_left",
      "keyboard_arrow_down",
      "keyboard_arrow_right",
    ];

    const ruKeyLayout = [
      "ё",
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "0",
      "-",
      "=",
      "backspace",
      "keyboard_capslock",
      "й",
      "ц",
      "у",
      "к",
      "е",
      "н",
      "г",
      "ш",
      "щ",
      "з",
      "х",
      "ъ",
      "delete",
      "unarchive",
      "ф",
      "ы",
      "в",
      "а",
      "п",
      "р",
      "о",
      "л",
      "д",
      "ж",
      "э",
      "\\",
      "keyboard_return",
      "keyboard_hide",
      "keyboard_voice",
      "я",
      "ч",
      "с",
      "м",
      "и",
      "т",
      "ь",
      "б",
      "ю",
      ".",
      "keyboard_arrow_up",
      "Ctrl",
      "Ru",
      "Alt",
      "space_bar",
      " ",
      "keyboard_arrow_left",
      "keyboard_arrow_down",
      "keyboard_arrow_right",
    ];

    const keyCodes = [
      "192",
      "49",
      "50",
      "51",
      "52",
      "53",
      "54",
      "55",
      "56",
      "57",
      "48",
      "189",
      "187",
      "8",
      "20",
      "81",
      "87",
      "69",
      "82",
      "84",
      "89",
      "85",
      "73",
      "79",
      "80",
      "219",
      "221",
      "46",
      "16",
      "65",
      "83",
      "68",
      "70",
      "71",
      "72",
      "74",
      "75",
      "76",
      "186",
      "222",
      "220",
      "13",
      "null",
      "null",
      "90",
      "88",
      "67",
      "86",
      "66",
      "78",
      "77",
      "188",
      "190",
      "191",
      "38",
      "17",
      "null",
      "18",
      "32",
      "null",
      "37",
      "40",
      "39",
    ];

    //
    const createIconHTML = (icon_name) => {
      return `<i class="material-icons">${icon_name}</i>`;
    };

    const createFirstLang = (letter) => {
      return `<span class="first-lang lang_active_true">${letter}</span>`;
    };

    const createSecondLang = (letter) => {
      return `<span class="second-lang lang_active_false">${letter}</span>`;
    };

    engKeyLayout.forEach((key, idx) => {
      const keyElement = document.createElement("button");
      const insertLineBreak =
        ["backspace", "delete", "keyboard_return", "keyboard_arrow_up"].indexOf(
          key
        ) !== -1;

      //setup key
      keyElement.setAttribute("type", "button");
      keyElement.classList.add("keyboard__key");
      keyElement.dataset.code = keyCodes[idx];

      switch (key) {
        case "backspace":
          keyElement.classList.add("keyboard__key_wide");
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._delLeft();
            this._triggerEvent("oninput");
            this._playSound(keyCodes[idx]);
          });

          break;

        case "volume_off":
          keyElement.classList.add(
            "keyboard__key_wide",
            "keyboard__key_activatable"
          );
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);
            this._volumeOff();
            keyElement.classList.toggle("keyboard__key_active");
          });

          break;

        case "delete":
          keyElement.classList.add("keyboard__key_wide");
          keyElement.innerHTML = "DELETE";
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._delRight();
            this._playSound(keyCodes[idx]);
          });

          break;

        case "keyboard_capslock":
          keyElement.classList.add(
            "keyboard__key_wide",
            "keyboard__key_activatable"
          );
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);
            this._toggleCapsLock();
            keyElement.classList.toggle("keyboard__key_active");
          });

          break;

        case "unarchive":
          keyElement.classList.add(
            "keyboard__key_wide",
            "keyboard__key_activatable"
          );
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);
            this._toggleShift();
            keyElement.classList.toggle("keyboard__key_active");
          });

          break;
        case "keyboard_voice":
          keyElement.classList.add(
            "keyboard__key_wide",
            "keyboard__key_activatable"
          );
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._speechRecognition();
            this._playSound(keyCodes[idx]);
            keyElement.classList.toggle("keyboard__key_active");
            keyElement.disabled = true;
            setTimeout(function () {
              keyElement.disabled = false;
            }, 1000);
          });

          break;

        case "keyboard_arrow_up":
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);

            this._move("up");
          });

          break;
        case "keyboard_arrow_left":
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);

            this._move(-1);
          });

          break;
        case "keyboard_arrow_down":
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);

            this._move("down");
          });

          break;
        case "keyboard_arrow_right":
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);

            this._move(1);
          });

          break;

        case "keyboard_return":
          keyElement.classList.add("keyboard__key_wide");
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "false";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);

            this._addNewLetter("\n");
            this._triggerEvent("oninput");
          });

          break;

        case "Ctrl":
          keyElement.classList.add(
            "keyboard__key_wide",
            "keyboard__key_activatable"
          );
          keyElement.textContent = key;
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);
            this._toggleCtrl();
            keyElement.classList.toggle("keyboard__key_active");
          });

          break;

        case "Eng":
          keyElement.innerHTML = key;
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);
            keyElement.innerHTML = this.properties.isMainLanguage
              ? `${ruKeyLayout[idx]}`
              : key;
            this._toggleLang();
          });

          break;

        case "Alt":
          keyElement.classList.add(
            "keyboard__key_wide",
            "keyboard__key_activatable"
          );
          keyElement.innerHTML = key;
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);
            this._toggleAlt();
            keyElement.classList.toggle("keyboard__key_active");
          });

          break;

        case "space_bar":
          keyElement.classList.add("keyboard__key_extrawide");
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "false";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);
            this._addNewLetter(" ");
            this._triggerEvent("oninput");
          });
          break;

        case "keyboard_hide":
          keyElement.classList.add("keyboard__key_wide", "keyboard__key_dark");
          keyElement.innerHTML = createIconHTML(key);
          keyElement.dataset.special = "true";

          keyElement.addEventListener("click", () => {
            this._playSound(keyCodes[idx]);
            this.close();
            this._triggerEvent("onclose");
            this.properties.input.blur();
          });
          break;

        default:
          keyElement.innerHTML =
            idx > 0 && idx < 13
              ? `${createFirstLang(key)}`
              : `${createFirstLang(key)} ${createSecondLang(ruKeyLayout[idx])}`;
          keyElement.dataset.special = "false";
          keyElement.addEventListener("click", () => {
            if (this.properties.isAdditionalValues && idx > 0 && idx < 11) {
              this._addNewLetter(
                this.properties.isMainLanguage
                  ? this.additionalEngLayout[idx - 1]
                  : this.additionalRuLayout[idx - 1]
              );
            } else {
              this._addNewLetter(
                this.properties.isMainLanguage
                  ? this.properties.isUpperCase
                    ? key.toUpperCase()
                    : key.toLowerCase()
                  : this.properties.isUpperCase
                  ? ruKeyLayout[idx].toUpperCase()
                  : ruKeyLayout[idx].toLowerCase()
              );
            }
            this._playSound(keyCodes[idx]);
            this._triggerEvent("oninput");
          });
      }

      fragment.appendChild(keyElement);
      if (insertLineBreak) {
        fragment.appendChild(document.createElement("br"));
      }
    });

    return fragment;
  },

  _playSound(keyCode) {
    if (!this.properties.isSoundOn) return;
    let audio = document.querySelector(`audio[data-sound="1"]`);
    if (!this.properties.isMainLanguage) {
      audio = document.querySelector(`audio[data-sound="3"]`);
    }
    const key = document.querySelector(`button[data-code="${keyCode}"]`);
    if (+keyCode === 20) {
      audio = document.querySelector(`audio[data-sound="2"]`);
    }
    if (+keyCode === 16) {
      audio = document.querySelector(`audio[data-sound="4"]`);
    }
    if (+keyCode === 13) {
      audio = document.querySelector(`audio[data-sound="5"]`);
    }
    if (+keyCode === 8) {
      audio = document.querySelector(`audio[data-sound="6"]`);
    }

    if (!audio) return;

    audio.currentTime = 0;
    audio.play();
  },

  _speechRecognition() {
    keyboard.properties.isSpeachRecognition = !keyboard.properties
      .isSpeachRecognition;
    recognition.lang = keyboard.properties.isMainLanguage ? "en-EN" : "ru-RU";
    let transcript = [];
    function recognitionToArea(e) {
      const flag = Array.from(e.results)[0].isFinal;
      transcript = Array.from(e.results)
        .map((result) => result[0])
        .map((result) => result.transcript)
        .join("");
      if (flag) {
        if (keyboard.properties.input.selectionStart > 0)
          keyboard._addNewLetter(" ");
        keyboard._addNewLetter(transcript);
        if (transcript.length !== 0) keyboard._addNewLetter(" ");
        transcript = [];
      }
    }

    if (!keyboard.properties.isSpeachRecognition) {
      recognition.onresult = null;
      recognition.onend = recognition.stop;
      recognition.stop();
      return;
    }

    recognition.onresult = recognitionToArea;
    recognition.onend = recognition.start;

    recognition.start();
  },

  _volumeOff() {
    this.properties.isSoundOn = !this.properties.isSoundOn;
  },

  _triggerEvent(handlerName) {
    if (typeof this.eventHandlers[handlerName] == "function") {
      this.eventHandlers[handlerName](this.properties.value);
    }
  },

  _toggleCapsLock() {
    this.properties.isUpperCase = !this.properties.isUpperCase;

    for (const key of this.elements.keys) {
      if (key.childElementCount === 2) {
        key.firstChild.textContent = this.properties.isUpperCase
          ? key.firstChild.textContent.toUpperCase()
          : key.firstChild.textContent.toLowerCase();
        key.lastChild.textContent = this.properties.isUpperCase
          ? key.lastChild.textContent.toUpperCase()
          : key.lastChild.textContent.toLowerCase();
      }
    }
  },

  _delLeft() {
    const start = this.properties.input.selectionStart;
    const text = this.properties.input.value;

    this.properties.value = text.slice(0, start - 1) + text.slice(start);
    this.properties.input.value = this.properties.value;
    this.properties.input.focus();
    this.properties.input.selectionStart = start - 1;
    this.properties.input.selectionEnd = start - 1;
  },

  _delRight() {
    this._move(1);
    this._delLeft();
  },

  _toggleShift() {
    this.properties.isUpperCase = !this.properties.isUpperCase;
    this.properties.isAdditionalValues = !this.properties.isAdditionalValues;
    // first 10 buttons - digits
    for (let i = 1; i < 11; i++) {
      if (this.properties.isAdditionalValues) {
        this.elements.keys[i].textContent = this.properties.isMainLanguage
          ? this.additionalEngLayout[i - 1]
          : this.additionalRuLayout[i - 1];
      } else {
        this.elements.keys[i].textContent = this.baseLayout[i - 1];
      }
    }

    for (const key of this.elements.keys) {
      if (key.childElementCount === 2) {
        key.firstChild.textContent = this.properties.isUpperCase
          ? key.firstChild.textContent.toUpperCase()
          : key.firstChild.textContent.toLowerCase();
        key.lastChild.textContent = this.properties.isUpperCase
          ? key.lastChild.textContent.toUpperCase()
          : key.lastChild.textContent.toLowerCase();
      }
    }
  },

  _toggleLang() {
    this.properties.isMainLanguage = !this.properties.isMainLanguage;
    for (let i = 1; i < 11; i++) {
      if (this.properties.isAdditionalValues) {
        this.elements.keys[i].textContent = this.properties.isMainLanguage
          ? this.additionalEngLayout[i - 1]
          : this.additionalRuLayout[i - 1];
      }
    }
    for (const key of this.elements.keys) {
      if (key.childElementCount === 2) {
        key.firstChild.classList.toggle("lang_active_true");
        key.firstChild.classList.toggle("lang_active_false");
        key.lastChild.classList.toggle("lang_active_false");
        key.lastChild.classList.toggle("lang_active_true");
      }
    }
  },

  _toggleAlt() {
    console.log("Error 404 =)");
  },

  _toggleCtrl() {
    console.log("Error 404 =)");
  },

  _move(direction) {
    const input = this.properties.input;
    const start = this.properties.input.selectionStart;
    if (direction === "up" || direction === "down") {
      console.log("Error 404 =)");
      return;
    }

    let newPosition = start + direction;
    if (newPosition < 0) newPosition = 0;

    input.focus();
    input.selectionStart = newPosition;
    input.selectionEnd = this.properties.input.selectionStart;

    return;
  },

  _addNewLetter(letter) {
    const start = this.properties.input.selectionStart;
    const text = this.properties.input.value;

    this.properties.value = text.slice(0, start) + letter + text.slice(start);
    this.properties.input.value = this.properties.value;
    this.properties.input.focus();
    this.properties.input.selectionStart = start + letter.length;
    this.properties.input.selectionEnd = this.properties.input.selectionStart;
  },

  open(initialValue, oninput, onclose, e) {
    this.properties.value = initialValue || "";
    this.eventHandlers.oninput = oninput;
    this.eventHandlers.onclose = onclose;

    this.elements.main.classList.remove("keyboard_hidden");
  },

  close() {
    this.properties.value = "";
    this.eventHandlers.oninput = oninput;
    this.eventHandlers.oninput = onclose;

    this.elements.main.classList.add("keyboard_hidden");
  },
};

window.addEventListener("DOMContentLoaded", () => {
  keyboard.init();
});

window.addEventListener("load", () => {
  if (document.body.offsetWidth > 960) {
  } else if (document.body.offsetWidth <= 960) {
    document.querySelector("button:nth-child(29)").textContent = "D";
  }
});

window.addEventListener("resize", () => {
  if (document.body.offsetWidth > 960) {
    document.querySelector("button:nth-child(29)").textContent = "DELETE";
  } else if (document.body.offsetWidth <= 960) {
    document.querySelector("button:nth-child(29)").textContent = "D";
  }
});

window.addEventListener("mousedown", (e) => {
  if (e.target.closest(".keyboard")) {
    e.preventDefault();
  }
});
