class Calculator {
  constructor(previousOperandTextElement, currentOperandTextElement) {
    this.previousOperandTextElement = previousOperandTextElement;
    this.currentOperandTextElement = currentOperandTextElement;
    this.needClear = false;
    this.clear();
  }

  clear() {
    this.currentOperand = '';
    this.previousOperand = '';
    this.operation = undefined;
    this.needClear = false;
  }

  delete() {
    this.currentOperand = this.currentOperand.toString().slice(0, -1);
  }

  appendNumder(number) {
    if (number === '.' && this.currentOperand.includes('.')) return;
    this.currentOperand = this.currentOperand.toString() + number.toString();
  }

  chooseOperation(operation) {
    if (this.currentOperand === '' && operation === '-') {
      this.appendNumder(operation);
      return;
    }
    if (this.currentOperand === '') return;
    if (this.previousOperand !== '') {
      this.compute();
    }
    this.operation = operation;
    this.previousOperand = this.currentOperand;
    this.currentOperand = '';
  }

  compute() {
    let computation;
    let prev = parseFloat(this.previousOperand);
    let current = parseFloat(this.currentOperand);
    if (isNaN(prev) || isNaN(current)) return;
    switch (this.operation) {
      case '+':
        computation = (prev + current).toFixed(7);
        break;
      case '-':
        computation = (prev - current).toFixed(7);
        break;
      case '*':
        computation = (prev * current).toFixed(7);
        break;
      case '÷':
        computation = current !== 0 ? (prev / current).toFixed(7) : 'ERROR';
        break;
      case 'xn':
        computation = (Math.pow(prev, current)).toFixed(7)
        break;
      default:
        return
    }
    this.currentOperand = this.getCropNumber(computation);
    this.operation = undefined;
    this.previousOperand = '';
    this.needClear = true;
  }

  getCropNumber(number) {
    number = number.replace(/0+$/, '');
    number = number.replace(/\.+$/, '');
    return number;
  }

  getDisplayNumber(number) {
    if (number === 'ERROR') return 'ERROR';
    if (number === '-') return '-';
    const stringNumber = number.toString();
    const integerDigits = parseFloat(stringNumber.split('.')[0]);
    const decimalDigits = stringNumber.split('.')[1];
    let integerDisplay
    if (isNaN(integerDigits)) {
      integerDisplay = '';
    } else {
      integerDisplay = integerDigits.toLocaleString('en', {
        maximumFractionDigits: 0
      })
    }
    if (decimalDigits != null) {
      return `${integerDisplay}.${decimalDigits}`
    } else {
      return integerDisplay
    }
  }

  updateDisplay() {
    this.currentOperandTextElement.innerText =
      this.getDisplayNumber(this.currentOperand)
    if (this.operation != null) {
      this.previousOperandTextElement.innerText =
        `${this.getDisplayNumber(this.previousOperand)}${this.operation !== 'xn' ? this.operation : '^'}`;
    } else {
      this.previousOperandTextElement.innerText = '';
    }

  }

  sqrtOperand() {
    this.currentOperand > 0 ?
      this.currentOperand = this.getCropNumber((Math.sqrt(this.currentOperand)).toFixed(7)) :
      this.currentOperand = 'ERROR';
    this.needClear = true;
  }

  negativeOperand() {
    if (this.currentOperand === '') {
      this.currentOperand = '-';
      return
    }
    this.currentOperand *= -1;
    this.needClear = true;
  }

  doMagic() {

    const hexValues = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e"];

    function seed(a) {
      for (var i = 0; i < 6; i++) {
        var x = Math.round(Math.random() * 14);
        var y = hexValues[x];
        a += y;
      }
      return a;
    }

    let newColor1 = seed('#');
    let newColor2 = seed('#');
    let angle = Math.round(Math.random() * 360);

    let gradient = `linear-gradient(${angle}deg, ${newColor1}, ${newColor2})`;
    document.body.style.background = gradient;
  }


}

const numberButtons = document.querySelectorAll('[data-number]');
const operationButtons = document.querySelectorAll('[data-operation]');
const equalsButton = document.querySelector('[data-equals]');
const deleteButton = document.querySelector('[data-delete]');
const allClearButton = document.querySelector('[data-all-clear]');
const previousOperandTextElement = document.querySelector('[data-previous-operand]');
const currentOperandTextElement = document.querySelector('[data-current-operand]');
const sqrtButton = document.querySelector('[data-sqrt]');
const minusButton = document.querySelector('[data-minus]');
const magicButton = document.querySelector('[data-magic]');

const calculator = new Calculator(previousOperandTextElement, currentOperandTextElement);

numberButtons.forEach((button) => {
  button.addEventListener('click', () => {
    if (calculator.previousOperand === '' &&
      calculator.currentOperand !== '' &&
      calculator.currentOperand !== '-' &&
      calculator.needClear) {
      calculator.currentOperand = '';
      calculator.needClear = false;
    }
    calculator.appendNumder(button.innerText);
    calculator.updateDisplay();
  })
})

operationButtons.forEach((button) => {
  button.addEventListener('click', () => {
    if (
      calculator.currentOperand === 'ERROR' && calculator.needClear) {
      calculator.currentOperand = '';
      calculator.needClear = false;
    }
    calculator.chooseOperation(button.innerText);
    calculator.updateDisplay();
  })
})

magicButton.addEventListener('click', () => {
  calculator.doMagic();
})

sqrtButton.addEventListener('click', () => {
  calculator.sqrtOperand();
  calculator.updateDisplay()
})

minusButton.addEventListener('click', () => {
  calculator.negativeOperand();
  calculator.updateDisplay();
})

equalsButton.addEventListener('click', (button) => {
  calculator.compute();
  calculator.updateDisplay();
})

allClearButton.addEventListener('click', (button) => {
  calculator.clear();
  calculator.updateDisplay();
})

deleteButton.addEventListener('click', (button) => {
  calculator.delete();
  calculator.updateDisplay();
})