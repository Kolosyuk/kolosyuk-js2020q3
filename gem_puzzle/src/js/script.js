import game from './game';
import menu from './menu';
import records from './records';

window.addEventListener('DOMContentLoaded', () => {
  menu.init();

  const resumeBtn = document.querySelector('#resume');
  const btnStart = document.querySelector('#start');
  const btnSave = document.querySelector('#save');
  const btnLoad = document.querySelector('#load');
  const btnBest = document.querySelector('#best');

  resumeBtn.addEventListener('click', (e) => {
    e.preventDefault();
    if (!localStorage.getItem('game_auto_state')) return;
    if (game.elements.main) {
      menu.elements.main.classList.add('main-menu_hidden');
      game.pauseGame(false);
    } else {
      game.loadAutoSaveFromStorage();
      game.init(game.properties.tableSide, '', game.properties.isPuzzleImage);
      menu.elements.main.classList.add('main-menu_hidden');
    }
  });

  btnStart.onclick = (e) => {
    e.preventDefault();
    const checkedDifficult = document.querySelector('input[name="field-size"]:checked');
    const checkedOption = document.querySelector('input[name="number-img"]:checked');
    if (game.elements.wrapper) {
      document.body.removeChild(game.elements.wrapper);
      document.body.className = '';
      game.clear();
    }
    game.init(+checkedDifficult.value, true, checkedOption.value);
    menu.elements.main.classList.add('main-menu_hidden');
  };

  btnSave.onclick = (e) => {
    e.preventDefault();
    if (!game.elements.main) return;

    game.saveToStorage();
  };

  btnLoad.onclick = (e) => {
    e.preventDefault();
    if (!localStorage.getItem('game_state')) return;
    if (game.elements.wrapper) {
      document.body.removeChild(game.elements.wrapper);
      document.body.className = '';
    }
    game.loadFromStorage();
    game.init(game.properties.tableSide, '', game.properties.isPuzzleImage);
    menu.elements.main.classList.add('main-menu_hidden');
  };

  btnBest.onclick = (e) => {
    e.preventDefault();
    records.renderResults();
  };
});

window.onbeforeunload = () => {
  if (game.elements.main) {
    game.avtoSaveToStorage();
  }
};

window.onresize = () => {
  if (game.elements.wrapper && game.properties.isPuzzleImage) {
    game.addBG();
  }
};
