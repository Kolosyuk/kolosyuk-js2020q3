import sound1 from '../audio/percussion-10.wav';
import sound2 from '../audio/percussion-12.wav';
import sound3 from '../audio/percussion-28.wav';
import sound4 from '../audio/percussion-50.wav';
import menu from './menu';
import records from './records';
import { MATRIX } from './const';

const game = {
  elements: {
    wrapper: null,
    main: null,
    gameInfo: null,
    gameTable: null,
    audio: null,
    menuCaller: null,
    soundSwitcher: null,
    autoSolver: null,
    gragTarget: null,
  },

  properties: {
    tableSide: 0,
    tableSize: 0,
    totalItems: [],
    goal: [],
    order: [],
    stack: [],
    time: '00:00',
    move: 0,
    emptyPosition: [],
    isPaused: false,
    isSoundOn: true,
    state: [],
    timer: null,
    isPuzzleImage: true,
    pathToPuzzleImage: null,
    dragShiftX: null,
    dragShiftY: null,
    autosolver: null,
  },

  init(n, flag = false, flagImgBg = false) {
    this.properties.tableSide = n;
    this.properties.tableSize = this.properties.tableSide ** 2;
    this.properties.isPuzzleImage = flagImgBg;

    // create game elements
    this.elements.wrapper = document.createElement('div');
    this.elements.main = document.createElement('div');
    this.elements.gameInfo = document.createElement('div');
    this.elements.gameTable = document.createElement('div');
    this.elements.menuCaller = document.createElement('div');
    this.elements.soundSwitcher = document.createElement('div');
    this.elements.autoSolver = document.createElement('div');

    //  setup
    this.elements.wrapper.classList.add('wrapper');
    this.elements.main.classList.add('game-wrapper');
    this.elements.gameInfo.classList.add('game-info');
    this.elements.gameTable.classList.add('game-table');
    this.elements.gameTable.classList.add(`table_size_${this.properties.tableSide}`);
    if (this.properties.isPuzzleImage) {
      this.elements.gameTable.classList.add('game-table_img_active');
    }
    this.elements.menuCaller.classList.add('game-wrapper__menu-caller');
    this.elements.soundSwitcher.classList.add('game-wrapper__sound-switcher');
    this.elements.autoSolver.classList.add('game-wrapper__auto-solver');

    //  add to dom
    document.body.appendChild(this.elements.wrapper);
    this.elements.wrapper.appendChild(this.elements.main);
    this.elements.main.appendChild(this.elements.gameInfo);
    this.renderGameInfo();
    this.elements.main.appendChild(this.elements.gameTable);
    this.elements.main.appendChild(this.elements.menuCaller);
    this.elements.main.appendChild(this.elements.soundSwitcher);
    this.elements.main.appendChild(this.elements.autoSolver);
    this.elements.main.appendChild(this.setAudio());
    if (this.properties.isPuzzleImage) {
      this.properties.pathToPuzzleImage = `url(${this.getRandomImage()})`;
    }
    this.startGame(flag);
    this.createMenuCaller();
    this.createAutoSolver();
    this.createSoundSwitcher();
  },

  createMenuCaller() {
    game.elements.menuCaller.insertAdjacentHTML('beforeend', '<button class="btn-menu">Menu</button>');
    game.elements.menuCaller.onclick = () => {
      if (!this.properties.isPaused) {
        this.pauseGame(false);
      }
      menu.elements.main.classList.toggle('main-menu_hidden');
      const input4 = document.querySelector('[value="4"]');
      input4.checked = true;
      const inputNumbers = document.querySelector('[name="number-img"]');
      inputNumbers.checked = true;
    };
  },

  createAutoSolver() {
    game.elements.autoSolver.insertAdjacentHTML('beforeend', '<button class="btn-menu">Solve IT!</button>');
    game.elements.autoSolver.onclick = () => {
      const pause = document.querySelector('#pause');
      game.elements.autoSolver.classList.toggle('game-wrapper__auto-solver_active');
      game.elements.autoSolver.onclick = null;
      game.elements.gameTable.onclick = null;
      clearTimeout(game.properties.timer);
      pause.onclick = null;
      this.autoSolve();
    };
  },

  startGame(flag) {
    clearTimeout(game.properties.autosolver);
    this.createTotalAmmount();
    if (flag) {
      this.elements.gameTable.appendChild(this.seedNewTable());
      document.body.classList.add(`table_size_${this.properties.tableSide}`);
      if (this.properties.isPuzzleImage) {
        this.addBG();
      }
      this.mixPieces(this.properties.tableSide * 30);
    } else {
      this.elements.gameTable.appendChild(this.seedSavedTable());
      document.body.classList.add(`table_size_${this.properties.tableSide}`);
      if (this.properties.isPuzzleImage) {
        this.addBG();
      }
    }

    this.startTimer();

    //  drag and drop
    this.elements.gameTable.onmousedown = function dragOn(event) {
      if (!event.target.classList.contains('table__item')) {
        return;
      }
      event.preventDefault();
      if (game.properties.isPaused) return;
      const piece = event.target.closest('.table__item');
      const pieceXpos = piece.style.gridColumnStart;
      const pieceYpos = piece.style.gridRowStart;
      const emptyCellColumnStart = game.properties.emptyPosition[0];
      const emptyCellRowStart = game.properties.emptyPosition[1];
      piece.ondragstart = function cancelDefaulеDnD() {
        return false;
      };

      if (
        (Math.abs(pieceXpos - emptyCellColumnStart) >= 1
        && Math.abs(pieceYpos - emptyCellRowStart) >= 1)
        || Math.abs(pieceXpos - emptyCellColumnStart) > 1
        || Math.abs(pieceYpos - emptyCellRowStart) > 1
      ) {
        return;
      }
      const clonedPiece = piece.cloneNode(true);
      game.elements.dragTarget = clonedPiece;
      document.body.appendChild(clonedPiece);
      piece.style.visibility = 'hidden';
      const shiftX = event.clientX - piece.getBoundingClientRect().left;
      const shiftY = event.clientY - piece.getBoundingClientRect().top;
      game.properties.dragShiftX = shiftX;
      game.properties.dragShiftY = shiftY;
      clonedPiece.style.boxShadow = '0 0 20px #fff';
      clonedPiece.style.position = 'absolute';
      clonedPiece.style.zIndex = 1000;

      game.moveAt(clonedPiece, event, shiftX, shiftY);

      document.addEventListener('mousemove', game.onMouseMove);

      clonedPiece.onmouseup = function gragOff() {
        document.removeEventListener('mousemove', game.onMouseMove);
        game.elements.dragTarget.style.boxShadow = '';
        piece.style.visibility = '';
        clonedPiece.remove();
        clonedPiece.onmouseup = null;
        game.moveItem(event);
      };
    };
  },

  onMouseMove(event) {
    game.moveAt(game.elements.dragTarget, event,
      game.properties.dragShiftX, game.properties.dragShiftY);
  },

  moveAt(item, event, shiftX, shiftY) {
    const clone = item;
    clone.style.left = `${event.pageX - shiftX}px`;
    clone.style.top = `${event.pageY - shiftY}px`;
  },

  seedNewTable() {
    const fragment = new DocumentFragment();
    let columnStep = 1;
    let rowStep = 1;
    this.properties.emptyPosition = [
      +this.properties.tableSide,
      +this.properties.tableSide,
    ];
    for (let i = 0; i < this.properties.tableSize - 1; i += 1) {
      this.properties.goal.push([columnStep, rowStep]);

      const piece = document.createElement('div');
      piece.className = 'table__item';
      piece.dataset.item = `${this.properties.totalItems[i]}`;
      piece.style.gridColumnStart = columnStep;
      piece.style.gridRowStart = rowStep;

      if (this.properties.isPuzzleImage) {
        piece.textContent = '';
        piece.style.backgroundImage = this.properties.pathToPuzzleImage;
        piece.style.backgroundSize = `${this.properties.tableSide * piece.getBoundingClientRect().width}px`;
        piece.style.backgroundPositionX = MATRIX[this.properties.tableSide][columnStep - 1];
        piece.style.backgroundPositionY = MATRIX[this.properties.tableSide][rowStep - 1];
      } else {
        piece.textContent = `${this.properties.totalItems[i]}`;
        piece.classList.add('table__item_rounded');
      }

      columnStep += 1;

      if (columnStep === +this.properties.tableSide + 1) {
        columnStep = 1;
        rowStep += 1;
      }
      fragment.appendChild(piece);
    }

    return fragment;
  },

  addBG() {
    let columnStep = 1;
    let rowStep = 1;
    const pieces = game.elements.gameTable.querySelectorAll('.table__item');

    for (let i = 0; i < game.properties.tableSize - 1; i += 1) {
      pieces[i].textContent = '';
      pieces[i].style.backgroundImage = game.properties.pathToPuzzleImage;
      pieces[i].style.backgroundSize = `${game.properties.tableSide * pieces[i].getBoundingClientRect().width}px`;
      pieces[i].style.backgroundPositionX = MATRIX[game.properties.tableSide][columnStep - 1];
      pieces[i].style.backgroundPositionY = MATRIX[game.properties.tableSide][rowStep - 1];

      columnStep += 1;

      if (columnStep === +game.properties.tableSide + 1) {
        columnStep = 1;
        rowStep += 1;
      }
    }
  },

  seedSavedTable() {
    const fragment = new DocumentFragment();

    for (let i = 0; i < this.properties.tableSize - 1; i += 1) {
      const piece = document.createElement('div');
      piece.textContent = game.properties.state[i].position;
      piece.className = 'table__item';
      piece.dataset.item = game.properties.state[i].position;
      piece.style.gridColumnStart = game.properties.state[i].positionX;
      piece.style.gridRowStart = game.properties.state[i].positionY;

      if (!this.properties.isPuzzleImage) {
        piece.classList.add('table__item_rounded');
      }

      fragment.appendChild(piece);
    }

    return fragment;
  },

  createTotalAmmount() {
    for (let i = 1; i <= this.properties.tableSize - 1; i += 1) {
      this.properties.totalItems.push(i);
    }
  },

  mixPieces(n = 500) {
    const { stack } = game.properties;
    for (let i = 0; i < n; i += 1) {
      const [x, y] = [this.properties.emptyPosition[0], this.properties.emptyPosition[1]];
      const elementStartPositions = this.randomizeDirect(x, y);
      const elementStartX = +elementStartPositions[0];
      const elementStartY = +elementStartPositions[1];
      const element = document.querySelector(`[style^="grid-column-start: ${elementStartX}; grid-row-start: ${elementStartY}"]`);

      if (i === 0) {
        stack.push([+element.dataset.item,
          +element.style.gridColumnStart, +element.style.gridRowStart]);
      } else if (+element.dataset.item === stack[stack.length - 1][0]) {
        stack.pop();
        i -= 2;
      } else {
        stack.push([+element.dataset.item,
          +element.style.gridColumnStart, +element.style.gridRowStart]);
      }

      this.properties.emptyPosition[0] = element.style.gridColumnStart;
      this.properties.emptyPosition[1] = element.style.gridRowStart;
      element.style.gridColumnStart = x;
      element.style.gridRowStart = y;
    }
  },

  randomizeDirect(x, y) {
    let columnStart = +x;
    let rowStart = +y;
    const tempArr = [];
    const a = Math.round(Math.random() * 1000 + 1);
    const b = Math.round(Math.random() * 1000 + 1);
    if (a >= b) {
      columnStart = a % 2 === 0 ? columnStart += 1 : columnStart -= 1;
    } else {
      rowStart = b % 2 === 0 ? rowStart += 1 : rowStart -= 1;
    }

    if (columnStart === 0) {
      columnStart += 2;
      tempArr.push(+columnStart);
      tempArr.push(+rowStart);
      return tempArr;
    }

    if (rowStart === 0) {
      rowStart += 2;
      tempArr.push(+columnStart);
      tempArr.push(+rowStart);
      return tempArr;
    }

    if (columnStart > +this.properties.tableSide) {
      columnStart -= 2;
      tempArr.push(+columnStart);
      tempArr.push(+rowStart);
      return tempArr;
    }

    if (rowStart > +this.properties.tableSide) {
      rowStart -= 2;
      tempArr.push(+columnStart);
      tempArr.push(+rowStart);
      return tempArr;
    }
    tempArr.push(+columnStart);
    tempArr.push(+rowStart);
    return tempArr;
  },

  autoSolve() {
    const x = game.properties.stack.pop();
    if (!x) {
      clearTimeout(game.autoSolve);
      game.win(true);
      return;
    }

    const id = x[0];
    const columnStart = x[1];
    const rowStart = x[2];
    const el = document.querySelector(`[data-item="${id}"]`);
    el.style.gridColumnStart = columnStart;
    el.style.gridRowStart = rowStart;

    game.playSound();
    game.properties.autosolver = setTimeout(game.autoSolve, 400);
  },

  saveToStorage() {
    localStorage.setItem('game_img', game.properties.isPuzzleImage);
    localStorage.setItem('game_totalItems', game.properties.totalItems);
    localStorage.setItem('game_order', game.properties.order);
    localStorage.setItem('game_emptyCell', game.properties.emptyPosition);
    localStorage.setItem('game_tableSide', game.properties.tableSide);
    localStorage.setItem('game_time', game.properties.time);
    localStorage.setItem('game_moves', game.properties.move);
    localStorage.setItem('game_state', JSON.stringify(this.createCurrentstate()));
    localStorage.setItem('game_stack', JSON.stringify(game.properties.stack));
  },

  avtoSaveToStorage() {
    localStorage.setItem('game_auto_img', game.properties.isPuzzleImage);
    localStorage.setItem('game_auto_totalItems', game.properties.totalItems);
    localStorage.setItem('game_auto_order', game.properties.order);
    localStorage.setItem('game_auto_emptyCell', game.properties.emptyPosition);
    localStorage.setItem('game_auto_tableSide', game.properties.tableSide);
    localStorage.setItem('game_auto_time', game.properties.time);
    localStorage.setItem('game_auto_moves', game.properties.move);
    localStorage.setItem('game_auto_state', JSON.stringify(this.createCurrentstate()));
    localStorage.setItem('game_auto_stack', JSON.stringify(game.properties.stack));
  },

  loadAutoSaveFromStorage() {
    game.properties.isPuzzleImage = localStorage.getItem('game_auto_img');
    game.properties.totalItems = localStorage.getItem('game_auto_totalItems').split(',');
    game.properties.order = localStorage.getItem('game_auto_order').split(',');
    game.properties.emptyPosition = localStorage.getItem('game_auto_emptyCell').split(',');
    game.properties.tableSide = +localStorage.getItem('game_auto_tableSide');
    game.properties.tableSize = game.properties.tableSide ** 2;
    game.properties.time = localStorage.getItem('game_auto_time');
    game.properties.move = +localStorage.getItem('game_auto_moves');
    game.properties.state = JSON.parse(localStorage.getItem('game_auto_state'));
    game.properties.stack = JSON.parse(localStorage.getItem('game_auto_stack'));
    game.properties.goal = game.createGoal();
  },

  loadFromStorage() {
    game.clear();
    game.properties.isPuzzleImage = localStorage.getItem('game_img');
    game.properties.totalItems = localStorage.getItem('game_totalItems').split(',');
    game.properties.order = localStorage.getItem('game_order').split(',');
    game.properties.emptyPosition = localStorage.getItem('game_emptyCell').split(',');
    game.properties.tableSide = +localStorage.getItem('game_tableSide');
    game.properties.tableSize = game.properties.tableSide ** 2;
    game.properties.time = localStorage.getItem('game_time');
    game.properties.move = +localStorage.getItem('game_moves');
    game.properties.state = JSON.parse(localStorage.getItem('game_state'));
    game.properties.stack = JSON.parse(localStorage.getItem('game_stack'));
    game.properties.goal = game.createGoal();
  },

  getRandomImage() {
    const randFile = Math.round(Math.random() * 150 + 1);
    const url = `./img/${randFile}.jpg`;
    return url;
  },

  createGoal() {
    const arr = [];
    let columnStep = 1;
    let rowStep = 1;
    for (let i = 0; i < game.properties.tableSize - 1; i += 1) {
      arr.push([columnStep, rowStep]);
      columnStep += 1;

      if (columnStep === +game.properties.tableSide + 1) {
        columnStep = 1;
        rowStep += 1;
      }
    }

    return arr;
  },

  createCurrentstate() {
    const currentState = Array.from(document.querySelectorAll('.table__item')).reduce((acc, item) => {
      acc.push({
        position: item.dataset.item,
        positionX: item.style.gridColumnStart,
        positionY: item.style.gridRowStart,
      });
      return acc;
    }, []);

    return currentState;
  },

  startTimer() {
    let min = +game.properties.time.substring(0, 2);
    let sec = +game.properties.time.substring(3);
    if (game.properties.isPaused) return;
    sec += 1;
    if (sec === 60) {
      sec = 0;
      min += 1;
      if (min === 60) {
        game.properties.time = 'Время замерло';
        clearTimeout(game.properties.timer);
        game.renderGameInfo();
        return;
      }
    }
    min = min >= 10 ? min : `0${min}`;
    sec = sec >= 10 ? sec : `0${sec}`;
    game.properties.time = `${min}:${sec}`;

    game.properties.timer = setTimeout(game.startTimer, 1000);
    game.renderGameInfo();
  },

  moveCounter() {
    this.properties.move = +this.properties.move + 1;
    this.renderGameInfo();
  },

  moveItem({ target }) {
    const targetElement = target;
    const { stack } = game.properties;
    if (game.properties.isPaused) return;
    if (target.closest('.table__item')) {
      const columnStart = targetElement.style.gridColumnStart;
      const rowStart = targetElement.style.gridRowStart;
      const emptyCellColumnStart = game.properties.emptyPosition[0];
      const emptyCellRowStart = game.properties.emptyPosition[1];

      if (
        (Math.abs(columnStart - emptyCellColumnStart) >= 1
        && Math.abs(rowStart - emptyCellRowStart) >= 1)
        || Math.abs(columnStart - emptyCellColumnStart) > 1
        || Math.abs(rowStart - emptyCellRowStart) > 1
      ) {
        return;
      }

      if (+targetElement.dataset.item === stack[stack.length - 1][0]) {
        stack.pop();
      } else {
        stack.push([+targetElement.dataset.item,
          +targetElement.style.gridColumnStart, +targetElement.style.gridRowStart]);
      }

      game.playSound();
      game.properties.emptyPosition.push(columnStart);
      game.properties.emptyPosition.push(rowStart);
      targetElement.style.gridColumnStart = game.properties.emptyPosition.shift();
      targetElement.style.gridRowStart = game.properties.emptyPosition.shift();
      game.moveCounter();
    }
    game.avtoSaveToStorage();
    game.checkWin();
  },

  checkWin() {
    const currentStateSorted = Array.from(document.querySelectorAll('.table__item')).sort(game.comparator);

    for (let i = 0; i < currentStateSorted.length; i += 1) {
      const elementColumn = +currentStateSorted[i].style.gridColumnStart;
      const elementRow = +currentStateSorted[i].style.gridRowStart;
      if (
        elementColumn === game.properties.goal[i][0]
        && elementRow === game.properties.goal[i][1]
      ) {
        if (i === currentStateSorted.length - 1) {
          game.win();
        }
      } else {
        return;
      }
    }
  },

  comparator(a, b) {
    if (+a.dataset.item > +b.dataset.item) {
      return 1;
    }
    return -1;
  },

  win(autoSolve = false) {
    game.properties.isPaused = !game.properties.isPaused;
    const winPopup = document.createElement('div');
    const currentGame = document.querySelector('body > div.wrapper');
    const points = game.points();
    document.body.appendChild(winPopup);
    winPopup.className = 'win-popup';

    if (autoSolve) {
      winPopup.insertAdjacentHTML(
        'beforeend',
        `
        <div class="win-popup__body">
          <h2 class="win-popup__title">Главное не победа!</h2>
          <p class="win-popup__text">Тренируйся усерднее и в следующй раз точно соберешь! </p>
          <button class="btn btn-best" id="bestScore">best results</button>
          <button class="btn btn-start" id="newGame"> wants more? </button>
        </div>
      `,
      );
    } else {
      winPopup.insertAdjacentHTML(
        'beforeend',
        `
        <div class="win-popup__body">
          <h2 class="win-popup__title">Победа!!!</h2>
          <p class="win-popup__text">
          Ура! Вы решили головоломку за ${game.properties.time} и ${game.properties.move} ходов</p>
          <p class="win-popup__text">Вы заработали ${points} очков</p>
          <p class="win-popup__text" contenteditable="true" id="name">[Введите ваше имя]</p>
          <button class="btn btn-best" id="bestScore">best results</button>
          <button class="btn btn-start" id="newGame"> wants more? </button>
        </div>
      `,
      );
      const name = winPopup.querySelector('#name');
      const flag = false;
      name.onclick = () => {
        name.textContent = '';
      };

      name.onkeypress = (e) => {
        if (e.keyCode === 13) {
          e.preventDefault();
          name.blur();
        }
      };

      name.onblur = () => {
        if (name.textContent.trim() === '') {
          name.textContent = '[Введите ваше имя]';
          return;
        }
        records.updateTable(points, name.textContent, flag);
      };
    }

    clearTimeout(game.properties.timer);
    currentGame.remove();
    game.clear();

    const newGame = winPopup.querySelector('#newGame');
    const btnBest = winPopup.querySelector('#bestScore');

    newGame.onclick = (e) => {
      e.preventDefault();
      winPopup.remove();
      records.properties.newRecord = true;
      menu.elements.main.classList.toggle('main-menu_hidden');
    };

    btnBest.onclick = (e) => {
      e.preventDefault();
      records.renderResults();
    };
  },

  points() {
    return Math.ceil(Math.random() * 1000 + 1);
  },

  renderGameInfo() {
    this.elements.gameInfo.textContent = '';
    this.properties.move = this.properties.move < 10 ? `0${+this.properties.move}` : +this.properties.move;
    this.elements.gameInfo.insertAdjacentHTML(
      'beforeend', `<p class="game-time">Время игры: <kbr><span>${this.properties.time}</span></kbr></p>
      <p class="moves">Ходов:${this.properties.move}</p>
      <p class="pause" id="pause">Пауза</p>
    `,
    );

    const pause = document.querySelector('#pause');
    pause.onclick = this.pauseGame;
  },

  pauseGame(flag = true) {
    game.properties.isPaused = !game.properties.isPaused;

    const pause = document.querySelector('#pause');
    if (flag) {
      pause.classList.toggle('pause_active');
    }

    if (!game.properties.isPaused) {
      game.startTimer();
    }
  },

  setAudio() {
    const fragment = new DocumentFragment();
    const sounds = [sound1, sound2, sound3, sound4];

    for (let i = 0; i <= sounds.length - 1; i += 1) {
      const track = document.createElement('audio');
      track.src = sounds[i];
      track.dataset.sound = `${i + 1}`;
      fragment.appendChild(track);
    }

    return fragment;
  },

  playSound() {
    if (!this.properties.isSoundOn) return;
    const audio = document.querySelector(`audio[data-sound="${[Math.floor(Math.random() * 4 + 1)]}"]`);
    if (!audio) return;

    audio.currentTime = 0;
    audio.play();
  },

  createSoundSwitcher() {
    this.elements.soundSwitcher.insertAdjacentHTML('beforeend', '<button class="btn-menu">Sound ON/OFF</button>');
    game.elements.soundSwitcher.onclick = () => {
      this.properties.isSoundOn = !this.properties.isSoundOn;
      this.elements.soundSwitcher.classList.toggle('game-wrapper__sound-switcher_mute_active');
    };
  },

  clear() {
    if (game.elements.wrapper) {
      game.elements.gameTable.onmousedown = null;
      game.elements.soundSwitcher.onclick = null;
      game.elements.autoSolver.onclick = null;
      game.elements.menuCaller.onclick = null;
    }
    localStorage.removeItem('game_auto_totalItems');
    localStorage.removeItem('game_auto_goal');
    localStorage.removeItem('game_auto_order');
    localStorage.removeItem('game_auto_emptyCell');
    localStorage.removeItem('game_auto_tableSide');
    localStorage.removeItem('game_auto_time');
    localStorage.removeItem('game_auto_moves');
    localStorage.removeItem('game_auto_state');
    localStorage.removeItem('game_auto_stack');
    this.elements.wrapper = null;
    this.elements.main = null;
    this.elements.gameInfo = null;
    this.elements.gameTable = null;
    this.elements.audio = null;
    this.elements.menuCaller = null;
    this.elements.soundSwitcher = null;
    this.elements.autoSolver = null;
    this.elements.gragTarget = null;
    this.properties.tableSide = 0;
    this.properties.tableSize = 0;
    this.properties.totalItems = [];
    this.properties.goal = [];
    this.properties.order = [];
    this.properties.time = '00:00';
    this.properties.move = 0;
    this.properties.emptyPosition = [];
    this.properties.isPaused = false;
    this.properties.isSoundOn = true;
    this.properties.state = [];
    this.properties.timer = null;
    this.properties.stack = [];
    this.properties.isPuzzleImage = true;
    this.properties.pathToPuzzleImage = null;
    this.properties.dragShiftX = null;
    this.properties.dragShiftY = null;
  },
};

export default game;
