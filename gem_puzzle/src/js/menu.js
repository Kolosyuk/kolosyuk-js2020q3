import { MAX_SIDE } from './const';

const menu = {
  elements: {
    main: null,
    mask: null,
    menuBody: null,
    title: null,
    burger: null,
    menuForm: null,
  },
  init() {
    //  create menu elements
    this.elements.main = document.createElement('div');
    this.elements.mask = document.createElement('div');
    this.elements.menuBody = document.createElement('div');
    this.elements.title = document.createElement('h1');
    this.elements.menuForm = document.createElement('form');

    //  setup
    this.elements.main.classList.add('main-menu');
    this.elements.mask.classList.add('main-menu__mask');
    this.elements.menuBody.classList.add('main-menu__body');
    this.elements.title.classList.add('title');
    this.elements.title.textContent = 'Gem PUZZLE';

    this.elements.menuForm.classList.add('body__form');

    //  add to dom
    document.body.appendChild(this.elements.main);
    this.elements.main.appendChild(this.elements.mask);
    this.elements.mask.appendChild(this.elements.menuBody);
    this.elements.menuBody.appendChild(this.elements.title);

    this.elements.menuBody.appendChild(this.elements.menuForm);
    this.createForm();
  },

  createForm() {
    this.elements.menuForm.insertAdjacentHTML(
      'beforeend',
      `
      <fieldset>
        <legend>game difficulty</legend>
        <ul id="menu__list"></ul>
      </fieldset>
      <fieldset>
        <legend>game type</legend>
        <label class = "label m5"><input type="radio" name="number-img" checked value=""/> Numbers </label>
        <label class = "label m5"><input type="radio" name="number-img" value="true"/> Picture </label>
      </fieldset>
      <button class="btn btn-best" id="resume">resume game</button>
      <button class="btn btn-start" id="start">start new game</button>
      <button class="btn btn-save" id="save">save game</button>
      <button class="btn btn-load" id="load">load saved game</button>
      <button class="btn btn-best" id="best">best results</button>
    `,
    );
    const menuList = document.querySelector('#menu__list');
    menuList.appendChild(this.createList(MAX_SIDE));
  },

  createList(numberOfVariants) {
    //  min amount of fields = 3*3

    const fragment = new DocumentFragment();
    if (numberOfVariants >= 3) {
      for (let i = 3; i <= numberOfVariants; i += 1) {
        const listElement = document.createElement('li');
        listElement.innerHTML = `<label class = "label"><input type="radio" name="field-size" value="${i}"/>Game field size - ${i}*${i}</label>`;
        if (i === 4) {
          listElement.firstChild.firstChild.setAttribute('checked', true);
        }
        fragment.appendChild(listElement);
      }
    }

    return fragment;
  },
};

export default menu;
