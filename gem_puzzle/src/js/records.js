const records = {

  properties: {
    newRecord: true,
    bestResults: [],
  },
  renderResults() {
    document.body.insertAdjacentHTML(
      'afterbegin',
      `
    <div class="wrapper__best-score">
      <div class="best-score__body">
        <h2 class="best-score__title"> Best RESULTS</h2>
        <div class="btn_close" id="close">Close</div>
        <div class="best-score__table">
          
        </div>
      </div>
    </div>
    `,
    );

    const closeBtn = document.querySelector('#close');
    const wrapperBestScore = document.querySelector('.wrapper__best-score');
    const bestScoreTablel = document.querySelector('.best-score__table');
    bestScoreTablel.appendChild(this.recordsList());

    closeBtn.onclick = () => {
      wrapperBestScore.remove();
    };
  },

  recordsList() {
    const bestResults = JSON.parse(localStorage.getItem('bestscore')) || [];
    bestResults.sort(this.recordsComparator);
    const fragment = new DocumentFragment();
    const head = document.createElement('div');
    head.className = 'table__row';
    head.innerHTML = `<div class="table__position">Позиция</div><div class="table__name">Имя</div>
    <div class="table__points">Очки</div>`;
    fragment.appendChild(head);
    for (let i = 0; i < bestResults.length; i += 1) {
      const element = document.createElement('div');
      element.className = 'table__row';
      element.innerHTML = `<div class="table__position">${i + 1}</div>
                          <div class="table__name">${bestResults[i].name}</div>
                          <div class="table__points">${bestResults[i].points}</div>`;
      fragment.appendChild(element);
      if (i === 9) break;
    }

    return fragment;
  },

  recordsComparator(a, b) {
    if (a.points < b.points) return 1;
    if (a.points > b.points) return -1;
    return 0;
  },

  updateTable(points, name) {
    this.properties.bestResults = JSON.parse(localStorage.getItem('bestscore')) || [];
    const results = this.properties.bestResults;
    if (this.properties.newRecord) {
      results.push({ points, name });
      localStorage.setItem('bestscore', JSON.stringify(results));
    }
    this.properties.newRecord = false;
  },
};

export default records;
