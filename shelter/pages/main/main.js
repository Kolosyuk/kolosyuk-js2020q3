const headerMenu = document.querySelector(".header");
const headerLogo = document.querySelector(".header__logo");
const headerBurger = document.querySelector(".header__burger");
const modalBurger = document.querySelector(".modal__burger");
const modal = document.querySelector(".modal");
const popupCross = document.querySelector(".popup__cross");
const popup = document.querySelector(".popup");
const popupBody = document.querySelector(".popup__body");
const petsSlider = document.querySelector(".pets__slider");
const slide = document.querySelector(".slide");
const arrowLeft = document.querySelector("#arrow-left");
const arrowRight = document.querySelector("#arrow-right");

let pets = []; // 8
let randomSortedList = []; // 8
let cards = [];
let needCards = 3;
let currentItem = 0;
let currentItemPlus = 1;
let currentItemPlusPlus = 2;
let isEnabled = true;

//get data from json

const getData = async function (url) {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Ошибка по адресу ${url}, 
						статус ошибки ${response.status}`);
  }
  return await response.json();
};

// create popup function

function renderPopup({
  name,
  img,
  type,
  breed,
  description,
  age,
  inoculations,
  diseases,
  parasites,
}) {
  const popupContent = document.createElement("div");
  popupContent.className = "popup__content";
  popupContent.insertAdjacentHTML(
    "beforeend",
    `
    
      <div class="popup__content-image">
          <img src="${img}" alt="${name}">
      </div>
      <div class="popup__content-body">
        <div class="popup__content-text">
          <div class="popup__content-header">
            <div class="popup__content-title">${name}</div>
            <div class="popup__content-type">${type} - ${breed}</div>
          </div>
          <div class="popup__content-description">${description}</div>
          <ul class="popup__list">
            <li><span>Age: ${age}</span> </li>
            <li><span>Inoculations: ${inoculations}</span></li>
            <li><span>Diseases: ${diseases}</span></li>
            <li><span>Parasites: ${parasites}</span></li>
          </ul>
        </div>
      </div>
	`
  );
  popupBody.insertAdjacentElement("beforeend", popupContent);
  popup.classList.add("popup_visible");
}

// create card function

function createCard({ name, img }, flag = false) {
  const card = document.createElement("div");
  card.dataset.name = name;
  card.className = flag ? "card active" : "card";
  card.insertAdjacentHTML(
    "beforeend",
    `
                    <div class="card__image">
                      <img src="${img}" alt="${name}">
                    </div>
                    <div class="card__name">
                      <h5>${name}</h5>
                    </div>
                    <div class="btn btn__link">Learn more</div>
	`
  );
  slide.insertAdjacentElement("beforeend", card);
}

//create popup

function createPopup(name) {
  getData("../../assets/pets.json").then((data) => {
    renderPopup(...data.filter((item) => item.name === name));
  });
}

//create random sorted list for 8 item from pets.json

getData("../../assets/pets.json").then((data) => {
  petsHTML = data;

  randomSortedList = (() => {
    let tempArr = [];

    for (let i = 0; i < 1; i++) {
      const newPets = petsHTML;

      for (let j = petsHTML.length; j > 0; j--) {
        let randInd = Math.floor(Math.random() * j);
        const randElem = newPets.splice(randInd, 1)[0];
        newPets.push(randElem);
      }

      tempArr = [...newPets];
    }

    return tempArr;
  })();

  randomSortedList.forEach((item, idx) => {
    if (idx < needCards) {
      createCard(item, true);
    } else {
      createCard(item);
    }
  });
  cards = document.querySelectorAll(".card");
  checkBodySize();
});

//check body size

function checkBodySize() {
  if (document.body.offsetWidth >= 1280) {
    if (needCards !== 3) refreshCardsClasses(3);
    needCards = 3;
  } else if (document.body.offsetWidth >= 768) {
    if (needCards !== 2) refreshCardsClasses(2);
    needCards = 2;
  } else {
    if (needCards !== 1) refreshCardsClasses(1);
    needCards = 1;
  }
}

//refresh card classes in slider

function refreshCardsClasses(n) {
  for (let i = 0; i < cards.length; i++) {
    cards[i].classList.remove("active");
  }
  for (let i = 0; i < n; i++) {
    cards[i].classList.add("active");
  }
  currentItem = 0;
}

function toggleBurgerMenu() {
  headerLogo.classList.toggle("invisible");
  headerBurger.classList.toggle("burger-rotate");
  modalBurger.classList.toggle("burger-rotate");
  modal.classList.toggle("modal-isOpen");
  document.body.style.overflow = "";
}

function changeCurrentItems(n) {
  currentItem = (n + cards.length) % cards.length;
  currentItemPlus = (n + 1 + cards.length) % cards.length;
  currentItemPlusPlus = (n + 2 + cards.length) % cards.length;
}

function hideItem(direction) {
  isEnabled = false;

  switch (needCards) {
    case 3:
      cards[currentItemPlusPlus].classList.add(direction);
      cards[currentItemPlusPlus].addEventListener("animationend", function () {
        this.classList.remove("active", direction);
      });
    case 2:
      cards[currentItemPlus].classList.add(direction);
      cards[currentItemPlus].addEventListener("animationend", function () {
        this.classList.remove("active", direction);
      });
    default:
      cards[currentItem].classList.add(direction);
      cards[currentItem].addEventListener("animationend", function () {
        this.classList.remove("active", direction);
      });
      break;
  }
}

function showItem(direction) {
  switch (needCards) {
    case 3:
      cards[currentItemPlusPlus].classList.add("next", direction);
      cards[currentItemPlusPlus].addEventListener("animationend", function () {
        this.classList.remove("next", direction);
        this.classList.add("active");
      });
    case 2:
      cards[currentItemPlus].classList.add("next", direction);
      cards[currentItemPlus].addEventListener("animationend", function () {
        this.classList.remove("next", direction);
        this.classList.add("active");
      });
    default:
      cards[currentItem].classList.add("next", direction);
      cards[currentItem].addEventListener("animationend", function () {
        this.classList.remove("next", direction);
        this.classList.add("active");
        isEnabled = true;
      });
      break;
  }
}

function previousItems(n) {
  hideItem("to-right");
  changeCurrentItems(n - needCards);
  showItem("from-left");
}

function nextItems(n) {
  hideItem("to-left");
  changeCurrentItems(n + needCards);
  showItem("from-right");
}

function removePopup() {
  const child = document.querySelector(".popup__content");
  popup.classList.remove("popup_visible");
  popupBody.removeChild(child);
}

function closePopup(event) {
  let cross = document.querySelector(".popup__cross");
  if (
    event.target.classList.contains("popup") ||
    event.target.classList.contains("popup__cross-line") ||
    event.target.closest(".popup__cross")
  ) {
    cross.classList.remove("hovered");
    removePopup();
    document.body.style.overflow = "";
  }
}

function interactivePopup(event) {
  if (event.type === "mouseover") {
    if (
      event.target.classList.contains("popup") ||
      event.target.classList.contains("popup__cross-line")
    ) {
      popupCross.classList.add("hovered");
    }
  } else {
    if (
      event.target.classList.contains("popup") ||
      event.target.classList.contains("popup__cross-line")
    ) {
      popupCross.classList.remove("hovered");
    }
  }
}

modalBurger.addEventListener("click", toggleBurgerMenu);

popup.addEventListener("mouseover", interactivePopup);
popup.addEventListener("mouseout", interactivePopup);

popup.addEventListener("click", closePopup);

petsSlider.addEventListener("click", (e) => {
  if (e.target.closest(".card")) {
    createPopup(e.target.closest(".card").dataset.name);
    document.body.style.overflow = "hidden";
  }
});

arrowLeft.addEventListener("click", function () {
  if (isEnabled) {
    previousItems(currentItem);
  }
});

arrowRight.addEventListener("click", function () {
  if (isEnabled) {
    nextItems(currentItem);
  }
});

window.addEventListener("resize", checkBodySize);

headerMenu.addEventListener("click", (e) => {
  let burger = e.target.closest(".header__burger");
  let logo = e.target.closest("#logo");
  let preLastLink = document.querySelector("nav > ul > li:nth-child(3) > a");
  let lastLink = document.querySelector("nav > ul > li:nth-child(4) > a");
  if (e.target === preLastLink || e.target === lastLink || logo) {
    e.preventDefault();
  }
  if (burger) {
    toggleBurgerMenu();
    document.body.style.overflow = "hidden";
  }
});

modal.addEventListener("click", (e) => {
  let preLastLink = document.querySelector(
    "body > div > div > nav > ul > li:nth-child(3) > a"
  );
  let lastLink = document.querySelector(
    "body > div > div > nav > ul > li:nth-child(4) > a"
  );
  if (e.target === preLastLink || e.target === lastLink) {
    e.preventDefault();
  }
  if (e.target.classList.contains("modal")) {
    toggleBurgerMenu();
  }
});
