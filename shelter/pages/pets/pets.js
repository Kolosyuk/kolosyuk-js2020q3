const headerMenu = document.querySelector(".header");
const headerBurger = document.querySelector(".header__burger");
const modalBurger = document.querySelector(".modal__burger");
const modal = document.querySelector(".modal");
const popupCross = document.querySelector(".popup__cross");
const popup = document.querySelector(".popup");
const popupBody = document.querySelector(".popup__body");
const petsList = document.querySelector(".pets__list");
const currentPageHTML = document.querySelector("#current-page");
const prevPageHTML = document.querySelector("#prev-page");
const prevPageAllHTML = document.querySelector("#prev-page-all");
const nextPageHTML = document.querySelector("#next-page");
const nextPageAllHTML = document.querySelector("#next-page-all");
const petsHTML = document.querySelector("#pets");
/********************* */

let currentPage = 0;
let itemsPerPage = 8;

// select first page
function selectFirstPage() {
  currentPage = 0;
  petsList.style.top = `calc(15px - ${petsHTML.offsetHeight * currentPage}px)`;
  currentPageHTML.innerText = (currentPage + 1).toString();
  prevPageHTML.classList.add("inactive");
  prevPageAllHTML.classList.add("inactive");
  nextPageHTML.classList.remove("inactive");
  nextPageAllHTML.classList.remove("inactive");
}

//select previous page

function selectPreviousPage() {
  if (currentPage > 0) {
    currentPage--;
    nextPageAllHTML.classList.remove("inactive");
    nextPageHTML.classList.remove("inactive");
  }
  if (currentPage === 0) {
    prevPageHTML.classList.add("inactive");
    prevPageAllHTML.classList.add("inactive");
  }
  petsList.style.top = `calc(15px - ${petsHTML.offsetHeight * currentPage}px)`;
  currentPageHTML.innerText = (currentPage + 1).toString();
}

//select next page
function selectNextPage() {
  if (currentPage < petsList.offsetHeight / petsHTML.offsetHeight - 1) {
    currentPage++;
  }
  if (currentPage > petsList.offsetHeight / petsHTML.offsetHeight - 1) {
    nextPageAllHTML.classList.add("inactive");
    nextPageHTML.classList.add("inactive");
  }

  prevPageHTML.classList.remove("inactive");
  prevPageAllHTML.classList.remove("inactive");
  petsList.style.top = `calc(15px - ${petsHTML.offsetHeight * currentPage}px)`;
  currentPageHTML.innerText = (currentPage + 1).toString();
}
//select last page

function selectLastPage() {
  if (itemsPerPage === 8) {
    currentPage = 5;
    petsList.style.top = `calc(15px - ${
      petsHTML.offsetHeight * currentPage
    }px)`;
    currentPageHTML.innerText = (currentPage + 1).toString();
    nextPageAllHTML.classList.add("inactive");
    nextPageHTML.classList.add("inactive");
    prevPageHTML.classList.remove("inactive");
    prevPageAllHTML.classList.remove("inactive");
  } else if (itemsPerPage === 6) {
    currentPage = 7;
    petsList.style.top = `calc(15px - ${
      petsHTML.offsetHeight * currentPage
    }px)`;
    currentPageHTML.innerText = (currentPage + 1).toString();
    nextPageAllHTML.classList.add("inactive");
    nextPageHTML.classList.add("inactive");
    prevPageHTML.classList.remove("inactive");
    prevPageAllHTML.classList.remove("inactive");
  } else {
    currentPage = 15;
    petsList.style.top = `calc(15px - ${
      petsHTML.offsetHeight * currentPage
    }px)`;
    currentPageHTML.innerText = (currentPage + 1).toString();
    nextPageAllHTML.classList.add("inactive");
    nextPageHTML.classList.add("inactive");
    prevPageHTML.classList.remove("inactive");
    prevPageAllHTML.classList.remove("inactive");
  }
}

prevPageHTML.addEventListener("click", selectPreviousPage);
prevPageAllHTML.addEventListener("click", selectFirstPage);
nextPageHTML.addEventListener("click", selectNextPage);
nextPageAllHTML.addEventListener("click", selectLastPage);

function checkItemsPerPage() {
  if (document.body.offsetWidth > 1279) {
    if (itemsPerPage !== 8) selectFirstPage();
    itemsPerPage = 8;
  } else if (document.body.offsetWidth > 767) {
    if (itemsPerPage !== 6) selectFirstPage();
    itemsPerPage = 6;
  } else {
    if (itemsPerPage !== 3) selectFirstPage();
    itemsPerPage = 3;
  }
}

window.addEventListener("resize", checkItemsPerPage);

/****************/

let pets = [];

//get data from json

const getData = async function (url) {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Ошибка по адресу ${url}, 
						статус ошибки ${response.status}`);
  }
  return await response.json();
};

// create card function

function createCard({ name, img }) {
  const card = document.createElement("div");
  card.className = "card";
  card.dataset.name = name;
  card.insertAdjacentHTML(
    "beforeend",
    `
                    <div class="card__image">
                      <img src="${img}" alt="${name}">
                    </div>
                    <div class="card__name">
                      <h5>${name}</h5>
                    </div>
                    <div class="btn btn__link">Learn more</div>
	`
  );
  petsList.insertAdjacentElement("afterbegin", card);

  return card;
}

// create popup function

function renderPopup({
  name,
  img,
  type,
  breed,
  description,
  age,
  inoculations,
  diseases,
  parasites,
}) {
  const popupContent = document.createElement("div");
  popupContent.className = "popup__content";
  popupContent.insertAdjacentHTML(
    "beforeend",
    `
    
      <div class="popup__content-image">
          <img src="${img}" alt="${name}">
      </div>
      <div class="popup__content-body">
        <div class="popup__content-text">
          <div class="popup__content-header">
            <div class="popup__content-title">${name}</div>
            <div class="popup__content-type">${type} - ${breed}</div>
          </div>
          <div class="popup__content-description">${description}</div>
          <ul class="popup__list">
            <li><span>Age: ${age}</span> </li>
            <li><span>Inoculations: ${inoculations}</span></li>
            <li><span>Diseases: ${diseases}</span></li>
            <li><span>Parasites: ${parasites}</span></li>
          </ul>
        </div>
      </div>
	`
  );
  popupBody.insertAdjacentElement("beforeend", popupContent);
  popup.classList.add("popup_visible");
}

//create list for 48 randon items without repeat

getData("../../assets/pets.json").then((data) => {
  pets = data;

  fullPetsList = (() => {
    let tempArr = [];

    for (let i = 0; i < 6; i++) {
      const newPets = pets;

      for (let j = pets.length; j > 0; j--) {
        let randInd = Math.floor(Math.random() * j);
        const randElem = newPets.splice(randInd, 1)[0];
        newPets.push(randElem);
      }

      tempArr = [...tempArr, ...newPets];
    }
    return tempArr;
  })();

  fullPetsList = sort863(fullPetsList);

  fullPetsList.forEach(createCard);

  currentPageHTML.innerText = (currentPage + 1).toString();
});

const sort863 = (list) => {
  let unique8List = [];
  let length = list.length;
  for (let i = 0; i < length / 8; i++) {
    const uniqueStepList = [];
    for (j = 0; j < list.length; j++) {
      if (uniqueStepList.length >= 8) {
        break;
      }
      const isUnique = !uniqueStepList.some((item) => {
        return item.name === list[j].name;
      });
      if (isUnique) {
        uniqueStepList.push(list[j]);
        list.splice(j, 1);
        j--;
      }
    }
    unique8List = [...unique8List, ...uniqueStepList];
  }
  list = unique8List;

  list = sort6recursively(list);

  return list;
};

const sort6recursively = (list) => {
  const length = list.length;

  for (let i = 0; i < length / 6; i++) {
    const stepList = list.slice(i * 6, i * 6 + 6);

    for (let j = 0; j < 6; j++) {
      const duplicatedItem = stepList.find((item, ind) => {
        return item.name === stepList[j].name && ind !== j;
      });

      if (duplicatedItem !== undefined) {
        const ind = i * 6 + j;
        const which8OfList = Math.trunc(ind / 8);

        list.splice(which8OfList * 8, 0, list.splice(ind, 1)[0]);

        sort6recursively(list);
      }
    }
  }

  return list;
};

function createPopup(name) {
  getData("../../assets/pets.json").then((data) => {
    renderPopup(...data.filter((item) => item.name === name));
  });
}

function toggleBurgerMenu() {
  headerBurger.classList.toggle("burger-rotate");
  modalBurger.classList.toggle("burger-rotate");
  modal.classList.toggle("modal-isOpen");
  document.body.style.overflow = "";
}

function removePopup() {
  const child = document.querySelector(".popup__content");
  popup.classList.remove("popup_visible");
  popupBody.removeChild(child);
}

function closePopup(event) {
  popupCross.classList.remove("hovered");
  if (
    event.target.classList.contains("popup") ||
    event.target.classList.contains("popup__cross-line") ||
    event.target.closest(".popup__cross")
  ) {
    removePopup();
    document.body.style.overflow = "";
  }
}

function interactivePopup(event) {
  let cross = document.querySelector(".popup__cross");
  if (event.type === "mouseover") {
    if (
      event.target.classList.contains("popup") ||
      event.target.classList.contains("popup__cross-line")
    ) {
      cross.classList.add("hovered");
    }
  } else {
    if (
      event.target.classList.contains("popup") ||
      event.target.classList.contains("popup__cross-line")
    ) {
      cross.classList.remove("hovered");
    }
  }
}

modalBurger.addEventListener("click", toggleBurgerMenu);

popup.addEventListener("mouseover", interactivePopup);
popup.addEventListener("mouseout", interactivePopup);
popup.addEventListener("click", closePopup);

petsList.addEventListener("click", (e) => {
  if (e.target.closest(".card")) {
    createPopup(e.target.closest(".card").dataset.name);
    document.body.style.overflow = "hidden";
  }
});

headerMenu.addEventListener("click", (e) => {
  let burger = e.target.closest(".header__burger");
  let logo = e.target.closest("#logo");
  let preLastLink = document.querySelector("nav > ul > li:nth-child(3) > a");
  let lastLink = document.querySelector("nav > ul > li:nth-child(4) > a");

  if (e.target === preLastLink || e.target === lastLink || logo) {
    e.preventDefault();
  }
  if (burger) {
    toggleBurgerMenu();
    document.body.style.overflow = "hidden";
  }
});

modal.addEventListener("click", (e) => {
  let preLastLink = document.querySelector(
    "body > div > div > nav > ul > li:nth-child(3) > a"
  );
  let lastLink = document.querySelector(
    "body > div > div > nav > ul > li:nth-child(4) > a"
  );
  if (e.target === preLastLink || e.target === lastLink) {
    e.preventDefault();
  }
  if (e.target.classList.contains("modal")) {
    toggleBurgerMenu();
  }
});
